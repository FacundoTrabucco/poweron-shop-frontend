import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor() {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(
      tap(
        event => {
          if (event instanceof HttpResponse) {
            if (event.body.message) {
              // Si la api envía un mensaje en una respuesta podemos utilizar alguna caja flotante con el texto
            }
          }
        },
        response => {
          // ¿Cómo podriamos mostrar el mensaje de error que envía la api?
        }
      )
    );
  }
}
